import 'package:driver_app/app/commons/constants.dart';
import 'package:driver_app/app/commons/exceptions/user_exception.dart';
import 'package:driver_app/app/commons/get_it.dart';
import 'package:driver_app/app/home/blocs/user/user_cubit.dart';
import 'package:driver_app/app/home/repositories/home_repository_interface.dart';
import 'package:driver_app/app/home/repositories/models/user_model.dart';
import 'package:driver_app/app/home/repositories/user_repository_interface.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class SpyUserRepositoryImpl extends Mock implements HomeRepositoryInterface {}

void main() {
  setGlobals();
  late UserCubit cubit;
  late UserRepositoryInterface repository;

  setUp(() {
    repository = SpyUserRepositoryImpl();
    cubit = UserCubit(userRepository: repository);
  });

  tearDown(() {
    cubit.close();
  });

  group('[getUser]', () {
    final user = UserModel.fromMap({
      'ID': '100',
      'data': {
        'first_name': 'Foo',
        'last_name': 'Bar',
      }
    });

    test('Should emit [UserLoadInProgress]', () {
      expect(cubit.state, isA<UserLoadInProgress>());
    });

    test('Should emit [UserLoadInProgress, UserLoadSuccess]', () async {
      when(repository.getUser()).thenAnswer((_) async => Future.value(user));

      expectLater(
        cubit.stream,
        emitsInOrder([
          isA<UserLoadInProgress>(),
          isA<UserLoadSuccess>()
              .having(
                (state) => state.user.id,
                'id',
                '100',
              )
              .having(
                (state) => state.user.firstName,
                'firstName',
                'Foo',
              )
              .having(
                (state) => state.user.lastName,
                'lastName',
                'Bar',
              )
              .having(
                (state) => state.user.fullName,
                'fullName',
                'Foo Bar',
              ),
        ]),
      );
      cubit.getUser();
    });

    test('Should emit [UserLoadInProgress, UserLoadError]', () async {
      const _errorMessage = 'Falha';
      when(repository.getUser()).thenThrow(UserException(_errorMessage));

      expectLater(
        cubit.stream,
        emitsInOrder([
          isA<UserLoadInProgress>(),
          isA<UserLoadError>().having(
            (state) => state.message,
            'message',
            _errorMessage,
          ),
        ]),
      );
      cubit.getUser();
    });

    test('Should emit [UserLoadInProgress, UserLoadError]', () async {
      when(repository.getUser()).thenThrow(Strings.generalErrorMessage);

      expectLater(
        cubit.stream,
        emitsInOrder([
          isA<UserLoadInProgress>(),
          isA<UserLoadError>().having(
            (state) => state.message,
            'message',
            Strings.generalErrorMessage,
          ),
        ]),
      );
      cubit.getUser();
    });
  });
}
