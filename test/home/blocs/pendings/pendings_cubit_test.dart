import 'package:driver_app/app/commons/constants.dart';
import 'package:driver_app/app/commons/exceptions/pendings_exception.dart';
import 'package:driver_app/app/commons/get_it.dart';
import 'package:driver_app/app/home/blocs/pendings/pendings_cubit.dart';
import 'package:driver_app/app/home/blocs/user/user_cubit.dart';
import 'package:driver_app/app/home/repositories/models/pendings_model.dart';
import 'package:driver_app/app/home/repositories/models/user_model.dart';
import 'package:driver_app/app/home/repositories/pendings_repository_interface.dart';
import 'package:driver_app/app/home/repositories/user_repository_interface.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class SpyPendingsRepository extends Mock
    implements PendingsRepositoryInterface {}

class SpyUserRepository extends Mock implements UserRepositoryInterface {}

void main() {
  setGlobals();
  late UserCubit userCubit;
  late UserRepositoryInterface userRepository;
  late PendingsCubit pendingsCubit;
  late PendingsRepositoryInterface pendingsRepository;

  setUp(() {
    userRepository = SpyUserRepository();
    userCubit = UserCubit(userRepository: userRepository);

    pendingsRepository = SpyPendingsRepository();
    pendingsCubit = PendingsCubit(
      listenable: userCubit,
      repository: pendingsRepository,
    );
  });

  tearDown(() {
    userCubit.close();
  });

  group('[getPendings]', () {
    const driverId = '123';
    final pendings = PendingsModel.fromMap({
      "blocklisted": false,
      "boxes": 0,
      "orders": [5739280, 5739103]
    });
    final user = UserModel.fromMap({
      'ID': driverId,
      'data': {
        'first_name': 'Foo',
        'last_name': 'Bar',
      }
    });

    test('Should emit [PendingsLoadInProgress]', () {
      expect(pendingsCubit.state, isA<PendingsLoadInProgress>());
    });

    test('Should emit [PendingsLoadInProgress, PendingsLoadSuccess]', () async {
      when(pendingsRepository.getPendings(driverId))
          .thenAnswer((_) async => Future.value(pendings));

      expectLater(
        pendingsCubit.stream,
        emitsInOrder([
          isA<PendingsLoadInProgress>(),
          isA<PendingsLoadSuccess>()
              .having(
                (state) => state.pendings.boxesCount,
                'boxesCount',
                0,
              )
              .having(
                (state) => state.pendings.orders.length,
                'orders length',
                2,
              )
              .having(
                (state) => state.hasPendings,
                'hasPendings',
                true,
              ),
        ]),
      );
      userCubit.emit(UserLoadSuccess(user));
    });

    test('Should emit [PendingsLoadInProgress, PendingsLoadError]', () async {
      const _errorMessage = 'Falha';
      when(pendingsRepository.getPendings(driverId))
          .thenThrow(PendingsException(_errorMessage));

      expectLater(
        pendingsCubit.stream,
        emitsInOrder([
          isA<PendingsLoadInProgress>(),
          isA<PendingsLoadError>().having(
            (state) => state.message,
            'message',
            _errorMessage,
          ),
        ]),
      );
      userCubit.emit(UserLoadSuccess(user));
    });
    test('Should emit [PendingsLoadInProgress, PendingsLoadError]', () async {
      when(pendingsRepository.getPendings(driverId))
          .thenThrow(Strings.generalErrorMessage);

      expectLater(
        pendingsCubit.stream,
        emitsInOrder([
          isA<PendingsLoadInProgress>(),
          isA<PendingsLoadError>().having(
            (state) => state.message,
            'message',
            Strings.generalErrorMessage,
          ),
        ]),
      );
      userCubit.emit(UserLoadSuccess(user));
    });
  });
}
