import 'package:driver_app/app/commons/constants.dart';
import 'package:driver_app/app/commons/exceptions/load_exception.dart';
import 'package:driver_app/app/commons/get_it.dart';
import 'package:driver_app/app/home/blocs/loads/loads_cubit.dart';
import 'package:driver_app/app/home/blocs/user/user_cubit.dart';
import 'package:driver_app/app/home/repositories/loads_repository_interface.dart';
import 'package:driver_app/app/home/repositories/models/loads_model.dart';
import 'package:driver_app/app/home/repositories/models/user_model.dart';
import 'package:driver_app/app/home/repositories/user_repository_interface.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class SpyLoadsRepository extends Mock implements LoadsRepositoryInterface {}

class SpyUserRepository extends Mock implements UserRepositoryInterface {}

void main() {
  setGlobals();
  late UserCubit userCubit;
  late UserRepositoryInterface userRepository;
  late LoadsCubit loadsCubit;
  late SpyLoadsRepository loadsRepository;

  setUp(() {
    userRepository = SpyUserRepository();
    userCubit = UserCubit(userRepository: userRepository);

    loadsRepository = SpyLoadsRepository();
    loadsCubit = LoadsCubit(
      listenable: userCubit,
      repository: loadsRepository,
    );
  });

  tearDown(() {
    userCubit.close();
  });

  group('[getLoads]', () {
    const driverId = '123';
    final loads = LoadsModel.fromMap({
      "items": [
        {
          "id": 98743590,
          "driver_id": 500,
          "loaded_at": "2021-10-08T18:01:36.650620+00:00",
          "accepted": false,
          "accepted_at": null,
          "dispatched_boxes": 2,
          "user_id": 500
        },
      ],
      "total": 17,
      "page": 0,
      "size": 50
    });
    final user = UserModel.fromMap({
      'ID': driverId,
      'data': {
        'first_name': 'Foo',
        'last_name': 'Bar',
      }
    });

    test('Should emit [LoadsLoadInProgress]', () {
      expect(loadsCubit.state, isA<LoadsLoadInProgress>());
    });

    test('Should emit [LoadsLoadInProgress, LoadsLoadSuccess]', () async {
      when(loadsRepository.getLoads(driverId))
          .thenAnswer((_) async => Future.value(loads));

      expectLater(
        loadsCubit.stream,
        emitsInOrder([
          isA<LoadsLoadInProgress>(),
          isA<LoadsLoadSuccess>()
              .having(
                (state) => state.loads.total,
                'total',
                17,
              )
              .having(
                (state) => state.loads.hasLoads,
                'hasLoads',
                true,
              )
        ]),
      );
      userCubit.emit(UserLoadSuccess(user));
    });

    test('Should emit [LoadsLoadInProgress, LoadsLoadError]', () async {
      const String _errorMessage = 'Error';
      when(loadsRepository.getLoads(driverId))
          .thenThrow(LoadException(_errorMessage));

      expectLater(
        loadsCubit.stream,
        emitsInOrder([
          isA<LoadsLoadInProgress>(),
          isA<LoadsLoadError>().having(
            (state) => state.message,
            'message',
            _errorMessage,
          ),
        ]),
      );
      userCubit.emit(UserLoadSuccess(user));
    });

    test('Should emit [LoadsLoadInProgress, LoadsLoadError]', () async {
      when(loadsRepository.getLoads(driverId))
          .thenThrow(Strings.generalErrorMessage);

      expectLater(
        loadsCubit.stream,
        emitsInOrder([
          isA<LoadsLoadInProgress>(),
          isA<LoadsLoadError>().having(
            (state) => state.message,
            'message',
            Strings.generalErrorMessage,
          ),
        ]),
      );
      userCubit.emit(UserLoadSuccess(user));
    });
  });
}
