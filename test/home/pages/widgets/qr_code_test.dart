import 'package:driver_app/app/home/blocs/user/user_cubit.dart';
import 'package:driver_app/app/home/pages/widgets/qr_code.dart';
import 'package:driver_app/app/home/repositories/home_repository.dart';
import 'package:driver_app/app/home/repositories/models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  final user = UserModel.fromMap({
    'ID': '500',
    'data': {
      'first_name': 'Foo',
      'last_name': 'Bar',
    }
  });

  testWidgets('Should show QrCode image', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: BlocProvider(
        create: (context) => UserCubit(userRepository: HomeRepository())
          ..emit(UserLoadSuccess(user)),
        child: const QrCode(),
      ),
    ));
  });

  final qrCodeFinder = find.byType(NetworkImage);
  expect(qrCodeFinder, findsOneWidget);
}
