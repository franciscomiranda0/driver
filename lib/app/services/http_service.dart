import 'dart:convert';

import 'package:driver_app/app/services/response.dart';
import 'package:http/http.dart' as http;

class HttpService {
  final _client = http.Client();

  Future<Response> get(
    String uri, {
    Map<String, dynamic>? params,
    Map<String, String>? header,
  }) async {
    if (params != null) uri += _getQueryFrom(params);

    final response = await _client.get(
      Uri.parse(uri),
      headers: header,
    );
    return _parseHttpResponse(response);
  }

  Response _parseHttpResponse(http.Response httpResponse) {
    late final Response response;
    try {
      response = Response(
        statusCode: httpResponse.statusCode,
        data: jsonDecode(httpResponse.body),
      );
    } on Exception catch (e) {
      response = Response(
        statusCode: 500,
        exception: e,
      );
    }
    return response;
  }

  String _getQueryFrom(Map<String, dynamic> params) {
    var query = '?';
    params.forEach((key, value) => query += '$key=$value&');
    return query;
  }
}
