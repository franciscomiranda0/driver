class Response {
  final Map<String, dynamic>? data;
  final Exception? exception;
  final int statusCode;

  Response({
    this.data,
    this.exception,
    required this.statusCode,
  });

  bool get isSuccessful => statusCode >= 200 && statusCode < 300;
}
