import 'package:driver_app/app/commons/constants.dart';
import 'package:driver_app/app/home/blocs/user/user_cubit.dart';
import 'package:driver_app/app/home/pages/widgets/greeting.dart';
import 'package:driver_app/app/home/pages/widgets/loads.dart';
import 'package:driver_app/app/home/pages/widgets/pendings.dart';
import 'package:driver_app/app/home/pages/widgets/qr_code.dart';
import 'package:driver_app/app/home/pages/widgets/refresh_bar.dart';
import 'package:driver_app/source/themes/source_colors.dart';
import 'package:driver_app/source/widgets/source_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () => _refreshPage(context),
            icon: SvgPicture.asset(
              Assets.refreshIcon,
              color: SourceColors.black,
            ),
          )
        ],
        backgroundColor: SourceColors.gray,
        centerTitle: true,
        elevation: 0,
        iconTheme: const IconThemeData(color: SourceColors.black),
        title: SourceText(
          Strings.appBarAppName,
          fontSize: 15,
          fontWeight: SourceFontWeight.bold,
        ),
      ),
      backgroundColor: SourceColors.white,
      drawer: const Drawer(),
      body: RefreshIndicator(
        color: SourceColors.blue,
        displacement: 32,
        onRefresh: () => _refreshPage(context),
        strokeWidth: 4,
        triggerMode: RefreshIndicatorTriggerMode.onEdge,
        child: ListView(
          physics: const AlwaysScrollableScrollPhysics(),
          shrinkWrap: true,
          children: [
            const RefreshBar(),
            const SizedBox(height: 20),
            const Greeting(),
            const SizedBox(height: 12),
            const Pendings(),
            const SizedBox(height: 12),
            Loads(),
            const SizedBox(height: 12),
            const QrCode(),
            const SizedBox(height: 12),
          ],
        ),
      ),
    );
  }

  Future<void> _refreshPage(BuildContext context) =>
      context.read<UserCubit>().getUser();
}
