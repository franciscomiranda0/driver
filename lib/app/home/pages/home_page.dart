import 'package:driver_app/app/home/blocs/error/error_cubit.dart';
import 'package:driver_app/app/home/blocs/loads/loads_cubit.dart';
import 'package:driver_app/app/home/blocs/pendings/pendings_cubit.dart';
import 'package:driver_app/app/home/blocs/user/user_cubit.dart';
import 'package:driver_app/app/home/pages/views/home_view.dart';
import 'package:driver_app/app/home/repositories/home_repository.dart';
import 'package:driver_app/source/themes/source_colors.dart';
import 'package:driver_app/source/widgets/source_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  static const route = 'home_page_route';
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => HomeRepository(),
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) =>
                UserCubit(userRepository: context.read<HomeRepository>())
                  ..getUser(),
          ),
          BlocProvider(
            lazy: false,
            create: (context) => PendingsCubit(
              listenable: context.read<UserCubit>(),
              repository: context.read<HomeRepository>(),
            ),
          ),
          BlocProvider(
            lazy: false,
            create: (context) => LoadsCubit(
              listenable: context.read<UserCubit>(),
              repository: context.read<HomeRepository>(),
            ),
          ),
          BlocProvider(
            lazy: false,
            create: (context) => ErrorCubit(
              listenables: [
                context.read<LoadsCubit>(),
                context.read<PendingsCubit>(),
                context.read<UserCubit>(),
              ],
            ),
          ),
        ],
        child: BlocListener<ErrorCubit, ErrorState>(
          listener: (context, state) {
            if (state is ErrorCatchSuccess) {
              _showSnackbar(context, state.message);
            }
          },
          child: const HomeView(),
        ),
      ),
    );
  }

  void _showSnackbar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        action:
            SnackBarAction(label: 'Fechar', onPressed: () => DoNothingAction()),
        backgroundColor: SourceColors.blue,
        behavior: SnackBarBehavior.floating,
        content: SourceText(
          message,
          color: SourceColors.white,
          fontWeight: SourceFontWeight.semibold,
          textAlign: TextAlign.center,
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
      ),
    );
  }
}
