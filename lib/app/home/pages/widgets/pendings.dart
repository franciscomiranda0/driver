import 'package:driver_app/app/home/blocs/pendings/pendings_cubit.dart';
import 'package:driver_app/app/home/pages/widgets/pendings_toothstick.dart';
import 'package:driver_app/source/themes/source_colors.dart';
import 'package:driver_app/source/widgets/source_card.dart';
import 'package:driver_app/source/widgets/source_heading.dart';
import 'package:driver_app/source/widgets/source_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Pendings extends StatelessWidget {
  const Pendings({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocSelector<PendingsCubit, PendingsState, bool>(
      selector: (state) =>
          state is PendingsLoadSuccess ? state.pendings.hasPendings : true,
      builder: (context, hasPendings) {
        return hasPendings
            ? const SizedBox()
            : Stack(
                alignment: Alignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: SourceCard(
                      color: SourceColors.green,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            SourceHeading(
                              contentSpacement: 4,
                              heading: SourceText(
                                'Carregamento liberado',
                                color: SourceColors.darkGreen,
                                fontSize: 14,
                                fontWeight: SourceFontWeight.bold,
                              ),
                              subheading: SourceText(
                                'Dirija-se ao setor responsável e faça um novo carregamento',
                                color: SourceColors.darkGreen,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  const PendingsToothStick()
                ],
              );
      },
    );
  }
}
