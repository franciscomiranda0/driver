import 'package:driver_app/app/home/blocs/loads/loads_cubit.dart';
import 'package:driver_app/source/themes/source_colors.dart';
import 'package:driver_app/source/widgets/source_card.dart';
import 'package:driver_app/source/widgets/source_flat_button.dart';
import 'package:driver_app/source/widgets/source_heading.dart';
import 'package:driver_app/source/widgets/source_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Loads extends StatelessWidget {
  final List<_ButtonModel> _buttons;

  Loads({Key? key})
      : _buttons = [
          _ButtonModel('Verificar carregamentos', () {}),
          _ButtonModel('Entregar pedidos', () {}),
          _ButtonModel('Consultar NFe', () {}),
        ],
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocSelector<LoadsCubit, LoadsState, bool>(
      selector: (state) => state.loads.hasLoads,
      builder: (context, hasLoads) {
        return hasLoads
            ? Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: SourceCard(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        const SizedBox(height: 8),
                        SourceHeading(
                          heading: SourceText(
                            'O que você deseja?',
                            fontSize: 16,
                            fontWeight: SourceFontWeight.bold,
                          ),
                          subheading: SourceText(
                            'Selecione a opção abaixo',
                            color: SourceColors.darkGray,
                            fontSize: 12,
                            fontWeight: SourceFontWeight.regular,
                          ),
                        ),
                        const SizedBox(height: 16),
                        ..._buttons.map(_buildButton),
                      ],
                    ),
                  ),
                ),
              )
            : const SizedBox();
      },
    );
  }

  Widget _buildButton(_ButtonModel button) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        SourceFlatButton(
          onPressed: button.callback,
          child: SourceText(
            button.label,
            color: SourceColors.white,
            fontSize: 16,
            fontWeight: SourceFontWeight.bold,
          ),
        ),
        const SizedBox(height: 20),
      ],
    );
  }
}

class _ButtonModel {
  final String label;
  final VoidCallback callback;

  const _ButtonModel(this.label, this.callback);
}
