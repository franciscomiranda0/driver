import 'package:driver_app/source/themes/source_colors.dart';
import 'package:flutter/material.dart';

class PendingsToothStick extends StatelessWidget {
  const PendingsToothStick({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      right: 0,
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: SourceColors.black,
                width: 3,
              ),
              color: SourceColors.white,
              shape: BoxShape.circle,
            ),
            height: 12,
            width: 12,
          ),
          Container(
            color: SourceColors.black,
            height: 3,
            width: 14,
          )
        ],
      ),
    );
  }
}
