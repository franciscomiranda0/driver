import 'package:driver_app/app/home/blocs/user/user_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RefreshBar extends StatelessWidget {
  const RefreshBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocSelector<UserCubit, UserState, bool>(
      selector: (state) => state is UserLoadInProgress,
      builder: (context, isLoading) {
        return isLoading ? const LinearProgressIndicator() : const SizedBox();
      },
    );
  }
}
