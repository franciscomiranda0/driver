import 'package:driver_app/app/home/blocs/user/user_cubit.dart';
import 'package:driver_app/source/widgets/source_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Greeting extends StatelessWidget {
  const Greeting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          BlocBuilder<UserCubit, UserState>(
            buildWhen: (_, state) => state is UserLoadSuccess,
            builder: (context, state) {
              return SourceText(
                'Olá, ${state.user.fullName}!',
                fontSize: 18,
                fontWeight: SourceFontWeight.bold,
              );
            },
          ),
          SourceText(
            'Tudo bem?',
            fontSize: 14,
            fontWeight: SourceFontWeight.semibold,
          ),
        ],
      ),
    );
  }
}
