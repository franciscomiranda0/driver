import 'package:driver_app/app/home/blocs/user/user_cubit.dart';
import 'package:driver_app/source/themes/source_colors.dart';
import 'package:driver_app/source/widgets/source_card.dart';
import 'package:driver_app/source/widgets/source_heading.dart';
import 'package:driver_app/source/widgets/source_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class QrCode extends StatelessWidget {
  const QrCode({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: SourceCard(
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  children: [
                    SourceHeading(
                      contentSpacement: 8,
                      heading: SourceText(
                        'Nº de identificação:',
                      ),
                      subheading: BlocSelector<UserCubit, UserState, String>(
                        selector: (state) => state.user.id,
                        builder: (context, id) {
                          return SourceText(
                            id,
                            fontSize: 24,
                            fontWeight: SourceFontWeight.bold,
                          );
                        },
                      ),
                    ),
                    SourceText(
                      'Utilize o QRCode\nsempre que for necessário confirmar\nsua identificação',
                      color: SourceColors.darkGray,
                      fontSize: 12,
                    ),
                  ],
                ),
              ),
              BlocSelector<UserCubit, UserState, String>(
                selector: (state) => state.user.id,
                builder: (context, driverId) {
                  return Expanded(
                    child: driverId.isNotEmpty
                        ? Image.network(
                            'https://southamerica-east1-facily-817c2.cloudfunctions.net/generate_qr_code?data=$driverId',
                            errorBuilder: (_, __, ___) => const SizedBox(),
                          )
                        : const SizedBox(),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
