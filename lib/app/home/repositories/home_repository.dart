import 'package:driver_app/app/commons/exceptions/load_exception.dart';
import 'package:driver_app/app/commons/exceptions/pendings_exception.dart';
import 'package:driver_app/app/commons/exceptions/user_exception.dart';
import 'package:driver_app/app/commons/get_it.dart';
import 'package:driver_app/app/commons/repository.dart';
import 'package:driver_app/app/home/repositories/api/home_endpoints.dart';
import 'package:driver_app/app/home/repositories/home_repository_interface.dart';
import 'package:driver_app/app/home/repositories/models/user_model.dart';
import 'package:driver_app/app/home/repositories/models/pendings_model.dart';
import 'package:driver_app/app/home/repositories/models/loads_model.dart';
import 'package:driver_app/app/services/http_service.dart';

class HomeRepository extends Repository implements HomeRepositoryInterface {
  final HttpService _httpService;

  HomeRepository() : _httpService = getIt.get<HttpService>();

  @override
  Future<LoadsModel> getLoads(String driverId) async {
    final response = await _httpService.get(
      getUriOf([HomeEndpoints.load]),
      header: await authenticatedHeader,
      params: {'driver_id': driverId},
    );
    if (response.isSuccessful) {
      return LoadsModel.fromMap(response.data!);
    } else {
      throw LoadException('Falha ao buscar carregamentos');
    }
  }

  @override
  Future<PendingsModel> getPendings(String driverId) async {
    final response = await _httpService.get(
      getUriOf([HomeEndpoints.pendency]),
      header: await authenticatedHeader,
      params: {'driver_id': driverId},
    );
    if (response.isSuccessful) {
      return PendingsModel.fromMap(response.data!);
    } else {
      throw PendingsException('Falha ao buscar pendências');
    }
  }

  @override
  Future<UserModel> getUser() async {
    final response = await _httpService.get(
      getUriOf([HomeEndpoints.user]),
      header: await authenticatedHeader,
    );
    if (response.isSuccessful) {
      return UserModel.fromMap(response.data!);
    } else {
      throw UserException('Falha ao buscar dados do usuário');
    }
  }
}
