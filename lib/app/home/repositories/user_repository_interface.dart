import 'package:driver_app/app/home/repositories/models/user_model.dart';

abstract class UserRepositoryInterface {
  Future<UserModel> getUser();
}
