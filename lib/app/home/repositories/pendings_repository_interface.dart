import 'package:driver_app/app/home/repositories/models/pendings_model.dart';

abstract class PendingsRepositoryInterface {
  Future<PendingsModel> getPendings(String driverId);
}
