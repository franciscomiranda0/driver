class LoadsModel {
  final int total;

  LoadsModel._(this.total);

  LoadsModel.initial() : total = 0;

  factory LoadsModel.fromMap(Map<String, dynamic> map) {
    return LoadsModel._(map['total']);
  }

  bool get hasLoads => total > 0;
}
