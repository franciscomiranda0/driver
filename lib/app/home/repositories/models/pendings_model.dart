class PendingsModel {
  final int boxesCount;
  final List<int> orders;

  PendingsModel._({
    required this.boxesCount,
    required this.orders,
  });

  PendingsModel.initial()
      : boxesCount = 0,
        orders = const [];

  factory PendingsModel.fromMap(Map<String, dynamic> map) {
    return PendingsModel._(
      boxesCount: map['boxes'],
      orders: List<int>.from(map['orders'].map((o) => o)),
    );
  }

  bool get hasPendings => boxesCount > 0 || orders.isNotEmpty;
}
