class UserModel {
  final String id;
  final String firstName;
  final String lastName;

  UserModel._({
    required this.id,
    required this.firstName,
    required this.lastName,
  });

  UserModel.initial()
      : id = '',
        firstName = '',
        lastName = '';

  factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel._(
      id: map['ID'],
      firstName: map['data']['first_name'],
      lastName: map['data']['last_name'],
    );
  }

  String get fullName => '$firstName $lastName';
}
