import 'package:driver_app/app/home/repositories/models/loads_model.dart';

abstract class LoadsRepositoryInterface {
  Future<LoadsModel> getLoads(String driverId);
}
