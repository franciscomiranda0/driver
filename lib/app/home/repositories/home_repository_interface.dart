import 'package:driver_app/app/home/repositories/loads_repository_interface.dart';
import 'package:driver_app/app/home/repositories/pendings_repository_interface.dart';
import 'package:driver_app/app/home/repositories/user_repository_interface.dart';

abstract class HomeRepositoryInterface
    implements
        UserRepositoryInterface,
        PendingsRepositoryInterface,
        LoadsRepositoryInterface {}
