abstract class HomeEndpoints {
  static String get load => 'load-truck';
  static String get pendency => 'driver-pendency';
  static String get user => 'users-me';
}
