import 'package:driver_app/app/commons/constants.dart';
import 'package:driver_app/app/commons/exceptions/user_exception.dart';
import 'package:driver_app/app/home/repositories/models/user_model.dart';
import 'package:driver_app/app/home/repositories/user_repository_interface.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'user_state.dart';

class UserCubit extends Cubit<UserState> {
  final UserRepositoryInterface userRepository;

  UserCubit({
    UserState? initialState,
    required this.userRepository,
  }) : super(initialState ?? UserLoadInProgress(UserModel.initial()));

  Future<void> getUser() async {
    emit(UserLoadInProgress(state.user));
    try {
      final user = await userRepository.getUser();
      emit(UserLoadSuccess(user));
    } on UserException catch (e) {
      emit(UserLoadError(state.user, message: e.toString()));
    } catch (_) {
      emit(UserLoadError(state.user, message: Strings.generalErrorMessage));
    }
  }
}
