part of 'user_cubit.dart';

@immutable
abstract class UserState {
  final UserModel user;

  const UserState(this.user);
}

class UserLoadInProgress extends UserState {
  const UserLoadInProgress(UserModel user) : super(user);
}

class UserLoadSuccess extends UserState {
  const UserLoadSuccess(UserModel user) : super(user);
}

class UserLoadError extends UserState {
  final String message;

  const UserLoadError(UserModel user, {required this.message}) : super(user);
}
