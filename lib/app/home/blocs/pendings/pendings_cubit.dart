import 'dart:async';

import 'package:driver_app/app/commons/constants.dart';
import 'package:driver_app/app/commons/exceptions/pendings_exception.dart';
import 'package:driver_app/app/home/blocs/user/user_cubit.dart';
import 'package:driver_app/app/home/repositories/models/pendings_model.dart';
import 'package:driver_app/app/home/repositories/pendings_repository_interface.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'pendings_state.dart';

class PendingsCubit extends Cubit<PendingsState> {
  final PendingsRepositoryInterface repository;
  late final StreamSubscription _cubitSubscription;

  PendingsCubit({
    PendingsState? initialState,
    required this.repository,
    required Cubit listenable,
  }) : super(initialState ??
            PendingsLoadInProgress(
              PendingsModel.initial(),
              hasPendings: true,
            )) {
    _cubitSubscription = listenable.stream.listen(cubitListener);
  }

  void cubitListener(state) {
    state is UserLoadSuccess ? _getPendings(state.user.id) : DoNothingAction();
  }

  Future<void> _getPendings(String driverId) async {
    emit(PendingsLoadInProgress(state.pendings));
    try {
      final pendings = await repository.getPendings(driverId);
      emit(PendingsLoadSuccess(pendings));
    } on PendingsException catch (e) {
      emit(PendingsLoadError(state.pendings, message: e.message));
    } catch (_) {
      emit(PendingsLoadError(state.pendings,
          message: Strings.generalErrorMessage));
    }
  }

  @override
  Future<void> close() {
    _cubitSubscription.cancel();
    return super.close();
  }
}
