part of 'pendings_cubit.dart';

@immutable
abstract class PendingsState {
  final PendingsModel pendings;
  final bool hasPendings;

  const PendingsState(this.pendings, this.hasPendings);
}

class PendingsLoadInProgress extends PendingsState {
  PendingsLoadInProgress(PendingsModel pendings, {bool? hasPendings})
      : super(pendings, hasPendings ?? pendings.hasPendings);
}

class PendingsLoadSuccess extends PendingsState {
  PendingsLoadSuccess(PendingsModel pendings, {bool? hasPendings})
      : super(pendings, hasPendings ?? pendings.hasPendings);
}

class PendingsLoadError extends PendingsState {
  final String message;

  PendingsLoadError(
    PendingsModel pendings, {
    required this.message,
    bool? hasPendings,
  }) : super(pendings, hasPendings ?? pendings.hasPendings);
}
