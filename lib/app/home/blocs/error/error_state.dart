part of 'error_cubit.dart';

@immutable
abstract class ErrorState {}

class ErrorInitial implements ErrorState {}

class ErrorCatchSuccess implements ErrorState {
  final String message;

  ErrorCatchSuccess(this.message);
}
