import 'dart:async';

import 'package:driver_app/app/home/blocs/loads/loads_cubit.dart'
    show LoadsLoadError;
import 'package:driver_app/app/home/blocs/pendings/pendings_cubit.dart'
    show PendingsLoadError;
import 'package:driver_app/app/home/blocs/user/user_cubit.dart'
    show UserLoadError;
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'error_state.dart';

class ErrorCubit extends Cubit<ErrorState> {
  late final List<StreamSubscription> _subscriptions;

  ErrorCubit({required List<Cubit> listenables}) : super(ErrorInitial()) {
    _subscriptions = listenables
        .map((cubit) => cubit.stream.listen(cubitsListener))
        .toList();
  }

  void cubitsListener(state) {
    _isErrorState(state)
        ? emit(ErrorCatchSuccess(state.message))
        : DoNothingAction();
  }

  bool _isErrorState(state) =>
      state is UserLoadError ||
      state is PendingsLoadError ||
      state is LoadsLoadError;

  @override
  Future<void> close() {
    _subscriptions.forEach(_cancel);
    return super.close();
  }

  void _cancel(StreamSubscription s) => s.cancel();
}
