part of 'loads_cubit.dart';

@immutable
abstract class LoadsState {
  final LoadsModel loads;

  const LoadsState(this.loads);
}

class LoadsLoadInProgress extends LoadsState {
  const LoadsLoadInProgress(LoadsModel loads) : super(loads);
}

class LoadsLoadSuccess extends LoadsState {
  const LoadsLoadSuccess(LoadsModel loads) : super(loads);
}

class LoadsLoadError extends LoadsState {
  final String message;

  const LoadsLoadError(LoadsModel loads, {required this.message})
      : super(loads);
}
