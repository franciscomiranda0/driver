import 'dart:async';

import 'package:driver_app/app/commons/constants.dart';
import 'package:driver_app/app/commons/exceptions/load_exception.dart';
import 'package:driver_app/app/home/blocs/user/user_cubit.dart';
import 'package:driver_app/app/home/repositories/loads_repository_interface.dart';
import 'package:driver_app/app/home/repositories/models/loads_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'loads_state.dart';

class LoadsCubit extends Cubit<LoadsState> {
  final LoadsRepositoryInterface repository;
  late final StreamSubscription _cubitSubscription;

  LoadsCubit({
    required this.repository,
    required Cubit listenable,
  }) : super(LoadsLoadInProgress(LoadsModel.initial())) {
    _cubitSubscription = listenable.stream.listen(cubitListener);
  }

  void cubitListener(state) {
    state is UserLoadSuccess ? _getLoads(state.user.id) : DoNothingAction();
  }

  Future<void> _getLoads(String driverId) async {
    emit(LoadsLoadInProgress(state.loads));
    try {
      final loads = await repository.getLoads(driverId);
      emit(LoadsLoadSuccess(loads));
    } on LoadException catch (e) {
      emit(LoadsLoadError(state.loads, message: e.message));
    } catch (_) {
      emit(LoadsLoadError(state.loads, message: Strings.generalErrorMessage));
    }
  }

  @override
  Future<void> close() {
    _cubitSubscription.cancel();
    return super.close();
  }
}
