class LoadException implements Exception {
  final String message;

  LoadException(this.message);

  @override
  String toString() {
    return message;
  }
}
