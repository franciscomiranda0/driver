class PendingsException implements Exception {
  final String message;

  PendingsException(this.message);

  @override
  String toString() {
    return message;
  }
}
