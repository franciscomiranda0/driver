import 'package:driver_app/app/commons/constants.dart';
import 'package:driver_app/app/commons/get_it.dart';
import 'package:driver_app/app/commons/storage_repository.dart';

abstract class Repository {
  String getUriOf(List<dynamic> addresses, {String? baseUrl}) {
    return addresses.fold(
      baseUrl ?? Strings.homologApi,
      (previousValue, current) => '$previousValue/$current',
    );
  }

  Future<Map<String, String>> get authenticatedHeader async => {
        'Authorization':
            'Bearer ${await getIt.get<StorageRepository>().token ?? ''}',
        'Content-Type': 'application/json',
      };
}
