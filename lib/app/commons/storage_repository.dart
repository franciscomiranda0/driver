import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class StorageRepository {
  static const _token = 'auth_token';
  final FlutterSecureStorage _secureStorage;

  StorageRepository() : _secureStorage = const FlutterSecureStorage() {
    setToken();
  }

  Future<String?> get token => _secureStorage.read(key: _token);

  Future<void> setToken() async => await _secureStorage.write(
      key: _token,
      value:
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3d3dy5tZXJ5dG8uY29tLmJyLyIsImlhdCI6MTYzMzUzMjMwOSwibmJmIjoxNjMzNTMyMzA5LCJleHAiOjE2MzQxMzcxMDksImRhdGEiOnsidXNlciI6eyJpZCI6IjUwMCJ9fX0.cckvTtvCETxRSjqXKTWZeDNUDD-Ntc-nDVJ0H40x8dw');
}
