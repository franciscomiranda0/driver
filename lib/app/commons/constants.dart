abstract class Assets {
  static const refreshIcon = 'assets/svg/icons/refresh.svg';
}

abstract class Strings {
  static const appBarAppName = 'Facily Driver';
  static const fontFamily = 'Poppins';
  static const generalErrorMessage = 'Falha inesperada, tente novamente';
  static const homologApi =
      'https://hmlg-logistics-mobile-driver-cnbi5ucx.ue.gateway.dev/api/v1';
  static const qrCodeApi =
      'https://southamerica-east1-facily-817c2.cloudfunctions.net';
}
