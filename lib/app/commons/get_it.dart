import 'package:driver_app/app/commons/storage_repository.dart';
import 'package:driver_app/app/services/http_service.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

void setGlobals() {
  getIt.registerLazySingleton<HttpService>(() => HttpService());
  getIt.registerLazySingleton<StorageRepository>(() => StorageRepository());
}
