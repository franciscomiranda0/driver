import 'package:driver_app/app/commons/constants.dart';
import 'package:driver_app/app/home/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DriverApp extends StatelessWidget {
  const DriverApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: HomePage.route,
      routes: {
        HomePage.route: (_) => const HomePage(),
      },
      theme: ThemeData(
        secondaryHeaderColor: Colors.amberAccent,
        fontFamily: Strings.fontFamily,
      ),
    );
  }
}
