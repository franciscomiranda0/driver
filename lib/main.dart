import 'package:driver_app/app/commons/get_it.dart';
import 'package:driver_app/app/driver_app.dart';
import 'package:flutter/material.dart';

void main() {
  setGlobals();
  runApp(const DriverApp());
}
