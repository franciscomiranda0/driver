import 'package:flutter/material.dart';

class SourceHeading extends StatelessWidget {
  final double? contentSpacement;
  final Widget heading;
  final Widget? subheading;

  const SourceHeading({
    Key? key,
    this.contentSpacement,
    required this.heading,
    this.subheading,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        heading,
        if (contentSpacement != null) SizedBox(height: contentSpacement),
        subheading ?? const SizedBox(),
      ],
    );
  }
}
