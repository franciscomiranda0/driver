import 'package:driver_app/source/themes/source_colors.dart';
import 'package:flutter/material.dart';

class SourceText extends StatelessWidget {
  final String content;
  final Color color;
  final double fontSize;
  late final FontWeight fontWeight;
  final TextAlign textAlign;

  SourceText(
    this.content, {
    Key? key,
    Color? color,
    double? fontSize,
    SourceFontWeight? fontWeight,
    TextAlign? textAlign,
  })  : color = color ?? SourceColors.black,
        fontSize = fontSize ?? 14,
        fontWeight = SourceFont(fontWeight ?? SourceFontWeight.regular).weight,
        textAlign = textAlign ?? TextAlign.left,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      content,
      style: TextStyle(
        color: color,
        fontSize: fontSize,
        fontWeight: fontWeight,
      ),
      textAlign: textAlign,
    );
  }
}

class SourceFont {
  final SourceFontWeight _weight;

  SourceFont(this._weight);

  FontWeight get weight {
    FontWeight weight;
    switch (_weight) {
      case SourceFontWeight.light:
        weight = FontWeight.w300;
        break;
      case SourceFontWeight.regular:
        weight = FontWeight.w400;
        break;
      case SourceFontWeight.semibold:
        weight = FontWeight.w600;
        break;
      case SourceFontWeight.bold:
        weight = FontWeight.w700;
        break;
      case SourceFontWeight.black:
        weight = FontWeight.w900;
        break;
      default:
        weight = FontWeight.w400;
    }
    return weight;
  }
}

enum SourceFontWeight {
  light,
  regular,
  semibold,
  bold,
  black,
}
