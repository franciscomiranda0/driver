import 'package:driver_app/source/themes/source_colors.dart';
import 'package:flutter/material.dart';

class SourceCard extends StatelessWidget {
  final Widget child;
  final Color? color;

  const SourceCard({
    Key? key,
    required this.child,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(cardColor: color ?? SourceColors.gray),
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: child,
        ),
      ),
    );
  }
}
