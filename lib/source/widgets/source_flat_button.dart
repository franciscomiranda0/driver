import 'package:driver_app/source/themes/source_colors.dart';
import 'package:flutter/material.dart';

class SourceFlatButton extends StatelessWidget {
  final Widget child;
  final VoidCallback onPressed;

  const SourceFlatButton({
    Key? key,
    required this.child,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(SourceColors.blue),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
        ),
        padding: MaterialStateProperty.all(const EdgeInsets.all(16)),
      ),
      child: child,
    );
  }
}
