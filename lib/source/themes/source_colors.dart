import 'package:flutter/material.dart';

abstract class SourceColors {
  static const Color black = Color(0xFF000000);
  static const Color blue = Color(0xFF2964F5);
  static const Color darkGreen = Color(0xFF11270b);
  static const Color darkGray = Color(0xFF80858D);
  static const Color gray = Color(0xFFF5F5F5);
  static const Color green = Color(0xFFA9E5AD);
  static const Color white = Color(0xFFFFFFFF);
}
